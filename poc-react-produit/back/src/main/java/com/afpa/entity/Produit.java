package com.afpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Produit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUIT_SEQ")
    private int id;
	
	@NotBlank
	private String label;
	
	@NotNull
	private Integer quantite;
	
	@NotNull
	private Long prix;
	
	
}

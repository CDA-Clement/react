package com.afpa.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.ProduitDto;
import com.afpa.service.IProduitService;

@RestController
public class ProduitController {

	@Autowired
	private IProduitService produitService;

	@GetMapping(value = "/produit")
	public List<ProduitDto> listProduit(ModelAndView mv, HttpServletRequest request) {
		return this.produitService.chercherToutesLesProduits();
	}

	@GetMapping("/produit/{id}")
	public ResponseEntity<?> showProduit(@PathVariable Integer id) {

		Optional<ProduitDto> produitOptional = this.produitService.findById(id);
		if (produitOptional.isPresent()) {

			return ResponseEntity.ok().body(produitOptional.get());
		} else {

			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Pas de produit avec cet id");
		}

	}
	

	@DeleteMapping("/produit/{id}")
	public ResponseEntity<?> deleteGroup(@PathVariable Integer id) {

		produitService.supprimerId(id);
		return ResponseEntity.ok().build();
	}
	

	@PostMapping("/produit")
	ResponseEntity<Integer> ajouter(@Valid @RequestBody ProduitDto prod) throws URISyntaxException {
		System.err.println("prod "+prod.getLabel());
		Integer result = produitService.ajouter(prod);
		return ResponseEntity.created(new URI("/produit/" + result))
				.body(result);
	}


	@PutMapping("edit/produit")
	ResponseEntity<Boolean> mettreAJourProduit(
			@Valid 
			@RequestBody ProduitDto prod
			) throws URISyntaxException {
		System.err.println(prod.toString());
		Integer result = produitService.ajouter(prod);
		System.err.println("test service edit");
		
		return null;	

	}
}
